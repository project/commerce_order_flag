Commerce order flag
-----------
Project URL: http://drupal.org/project/commerce_order_class

* Introduction
-First at all thank you for using "Commerce order flag
". This module provides configurable TRUE/FALSE flag
for individual Commerce orders with possibility of quick bulk edit of values.
You can mark your orders with additional information
(for sample: 'was sent to client') without need of creating new custom bulk
 actions and attaching taxonomy term to existing order types.

* Requirements
-This module require commerce order,  which is part of Drupal Commerce project
(https://www.drupal.org/project/commerce)

* Installation
-Simply enable module in extension tab and you are ready to go.

* Configuration
-Set permissions for commerce order flag (/admin/people/permissions).

-Configure labels for flag and bulk actions
 in (/admin/structure/commerce_order_flag/settings).

-In view fields section, add flag field to display
 in (/admin/structure/views/view/commerce_orders).

-Optionaly clear system cache and add translation for labels
 put into module settings section.

* Maintainers
- dworzecki (https://www.drupal.org/u/dworzecki)
